var baseUrl = 'http://localhost/e-stockist/';
// var baseUrl = 'http://www.nini-store.com/';

$(document).ready(function(){
    $("img.lazy").lazyload(/*{
        effect : "fadeIn"
    }*/);

    $('.carousel').carousel({
      interval: 10000
    });

	$('.carousel').bind('slid.bs.carousel', function (e) {
    	$(".carousel img.lazy").lazyload(/*{
	       /* effect : "fadeIn"
	    }*/);
    });
});


//==================
//Live chat
//==================
var userName = "";
var timeout;
function submitLiveChat() {
    appendMessage();
    return false;
}
function initialize() {
    clearTimeout(timeout);
    if(userName == "") {
        timeout = setTimeout(function () {
            $.get( baseUrl+ "livechat/initialize", function( data ) {
                userName = data;
                $("#live-chat-from").html(userName);
                $("#live-chat").show();
                updateMessage();
            });
        }, 1000);
    } else {
        $("#live-chat-from").html(userName); 
        $("#live-chat").show();
        updateMessage();
    }
}
function forceClose() {
    clearTimeout(timeout);
    if(userName != "") {
        timeout = setTimeout(function () {
            $.get( baseUrl + "livechat/closeLiveChat/" + userName, function( data ) {
                userName = "";
                $("#live-chat-content").html("");
                $("#live-chat").hide();
                alert(data);
            });
        }, 500);
    }
}
function appendMessage() {
    if($("#live-chat-message-field").val() == "") return;
    clearTimeout(timeout);
    var message = $("#live-chat-message-field").val() + "\n";
    message = $("#live-chat-from").html() + " : " + message; 
    $("#live-chat-message-field").val("");
    if(userName != "") {
        timeout = setTimeout(function () {
            $.post( baseUrl + "livechat/addMessage/" + userName, {
                message : message
            }).done(function( data ) {
                $content = $("#live-chat-content");
                $content.html(data);
                $content.scrollTop($content[0].scrollHeight);
            });
            updateMessage();
        }, 0);
    } else {
        alert("Error when sending message to our staff !");
    }
}
function updateMessage(){
    clearTimeout(timeout);
    if(userName != "") {
        timeout = setTimeout(function () {
            $.get( baseUrl + "livechat/updateContent/" + userName, function( data ) {
                $content = $("#live-chat-content");
                $content.html(data);
                $content.scrollTop($content[0].scrollHeight);
            });
            updateMessage();
        }, 2000);
    }
}

$(".live-chat").click(function(){
    if($("#live-chat").is(":visible")) {
        $("#live-chat").hide();
    } else {
        initialize();
    }
});
$("#live-chat-min").click(function(){
    $("#live-chat").hide();
});
$("#live-chat-close").click(function(){
    //forceClose();
    $("#live-chat").hide();
});
//==================
//End of live chat
//==================