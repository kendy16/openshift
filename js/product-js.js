window.onload = function(){
	$productCount = $(".product-item").length;
	$indexPreview = 0;

	$(".product-item").click(function() {
		$indexPreview = $(".product-item").index($(this));
		updateModalAndShow();
	});

	function updateModalAndShow(idx) {
		$object = $(".product-item").eq($indexPreview);
		$content = $object.html();
		$("#productModal").find(".modal-body").html($content);
		$imageUrl = $("#productModal").find("img.lazy").attr("data-original");
		$("#productModal").find("img.lazy").attr("src", $imageUrl);
		$("#productModal").modal('show');
	}

	$(document).keydown(function(e) {
		if ($("#productModal").css('display') == "block") {
	    switch(e.which) {
	        case 37: 
	        if ($indexPreview > 0) {
	        	$indexPreview--;
	        	updateModalAndShow();
	        }
	        break;

	        case 38: // up
	        break;

	        case 39: 
	        if ($indexPreview < $productCount - 1) {
	        	$indexPreview++;
	        	updateModalAndShow();
	        }
	        break;

	        case 40: // down
	        break;

	        default: return; // exit this handler for other keys
	    }
		}

		return;
	    e.preventDefault(); // prevent the default action (scroll / move caret)
	});

};