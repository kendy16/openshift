var baseUrl = 'http://localhost/e-stockist/';
// var baseUrl = 'http://www.nini-store.com/';
//JS DataTable
oTable = null;

//==============ckfinder and editor=============
function BrowseServer() {
// You can use the "CKFinder" class to render CKFinder in a page:
    var finder = new CKFinder();
    finder.basePath = '/ckfinder/'; // The path for the installation of CKFinder (default = "/ckfinder/").
    finder.selectActionFunction = SetFileField;
    finder.popup();

    // It can also be done in a single line, calling the "static"
    // popup( basePath, width, height, selectFunction ) function:
    // CKFinder.popup( '../', null, null, SetFileField ) ;
    //
    // The "popup" function can also accept an object as the only argument.
    // CKFinder.popup( { basePath : '../', selectActionFunction : SetFileField } ) ;
}
CKFinder.setupCKEditor(null, baseUrl+"js/admin/ckfinder");
// This is a sample function which is called when a file is selected in CKFinder.
function SetFileField( fileUrl )
{
    document.getElementById( 'imageBrowser' ).value = fileUrl;
    document.getElementById( 'imagePreview' ).src = fileUrl;
}
//==============================================

$(document).ready(function(){
    $("img.lazy").lazyload({
        effect : "fadeIn"
    });
            
    if($('.showConfirm')){
        $('.showConfirm').click(function(event){
            event.preventDefault();
            if(confirm($(this).attr('title'))){
                window.location.href =  $(this).attr('href');
            }
        })
    }

    if($('.dtTables').find('tr').length >=1){
        oTable = $('.dtTables').not('.dtInfoTables').not('.dtPlainTables').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "aLengthMenu": [[10, 25, 50, 100, 200, -1], [10, 25, 50, 100, 200, "All"]],
            "iDisplayLength": 200,
            "aoColumnDefs":[{'bSortable':false,'aTargets':['hPrint']}]
        });
        if($('.dtTables .defSortAsc').length>0){
            oTable.fnSort([[$('.dtTables .defSortAsc').index(),'asc']]);
        }
        if($('.dtTables .defSortDesc').length>0){
            oTable.fnSort([[$('.dtTables .defSortDesc').index(),'desc']]);
        }
        oTable.removeAttr('style');
    }
    if($('.dtInfoTables').find('tr').length >=1){
        oTable = $('.dtInfoTables').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers",
            "bFilter": false,
            "bSort": false,
            "bInfo": false,
            "bPaginate": false
        });
        oTable.removeAttr('style');
    }
    
    $('.dPicker').datepicker({
        showButtonPanel:true,
        changeMonth:true,
        changeYear:true,
        dateFormat:'yy-mm-dd'
    });

    $('.integerOnly').keypress(function(e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        if (!($.inArray(k,a)>=0))
            e.preventDefault();
    });

    $('.floatOnly').keypress(function(e) {
        var a = [];
        var k = e.which;

        for (i = 48; i < 58; i++)
            a.push(i);

        //for point
        a.push(46);
        if ($(this).val().indexOf(".") >= 0 && k == 46) {
            //already has .
            return false;
        }

        if (!($.inArray(k,a)>=0))
            e.preventDefault();
    });
});