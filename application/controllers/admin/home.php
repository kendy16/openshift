<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	function __construct(){
		parent::__construct();
        $this->pageName = $this->projectName." - Dashboard";
        $this->headerNavigation = "Dashboard";
    }
    public function index($isModal = 1)
    {
        $this->load->model("kenmodel");
		$tableName = "product";

        // show only product active
		$products = $this->kenmodel->select($tableName);
		// $totalCollect
		$totalCollect = 0;

		foreach ($products as $product) {
			$buys = $this->kenmodel->selectTotalQuantity("buy", "fk_product_id = $product->product_id");
			$sells = $this->kenmodel->selectTotalQuantity("sell", "fk_product_id = $product->product_id");

			$uncollected = $this->kenmodel->selectTotalQuantity("sell", "fk_product_id = $product->product_id and isCollected = 0");
			$uncollectedAmount = $this->kenmodel->selectTotalPrice("sell", "fk_product_id = $product->product_id and isCollected = 0");
			$totalUntung = $uncollectedAmount - ($uncollected * $product->product_capital_price);
			$totalUntung = max($totalUntung, 0);
			
			$totalCollect += $totalUntung;

			
			$product->buys = $buys;
			$product->sells = $sells;
			$product->balance = $buys - $sells;
		}

		// fields
		$fields = $this->db->field_data($tableName);
		$filterFields = array('product_id', 'product_title', 'product_price');
		$displayField = array();
		$index = 0;
		foreach ($fields as $field) {
			if (in_array($field->name, $filterFields)) {
				$displayField[$index] = $field;
			}
			$index++;
		}

		/*
		$temp = new stdClass();
		$temp->name = "buys";
		$temp->type = "int";
		$displayField["buys"] = $temp;
		*/
		
		$temp = new stdClass();
		$temp->name = "sells";
		$temp->type = "int";
		$displayField["sells"] = $temp;

		$temp = new stdClass();
		$temp->name = "balance";
		$temp->type = "int";
		$displayField["balance"] = $temp;

		$temp = new stdClass();
		$temp->name = "buy / sell";
		$temp->type = "custom";
		$displayField["custom"] = $temp;
		
		$this->templateAdmin('admin/panel.php', array('data' => $products, 'fields' => $displayField, 'tableName' => 'dashboard', 'deleteAble' => 0, 'switchAble' => 0, 'actionAble' => 0, 
"totalCollect" => $totalCollect, "isModal" => $isModal));
        // $this->templateAdmin('admin/dashboard.php', array('companyName'=> $this->companyName));
    }

    public function downloadSoldItem(){
 		$this->load->model("kenmodel");	
	
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		
		// show only product active
		$products = $this->kenmodel->select('product');
		// $totalCollect
		$totalCollect = 0;
		$totalModal = 0;

		foreach ($products as $product) {
			$uncollected = $this->kenmodel->selectTotalQuantity("sell", "fk_product_id = $product->product_id and isCollected = 0");
			$uncollectedAmount = $this->kenmodel->selectTotalPrice("sell", "fk_product_id = $product->product_id and isCollected = 0");
			$modal = ($uncollected * $product->product_capital_price);
			$collected = $uncollectedAmount - $modal;
			$collected = max($collected, 0);
			
			$totalCollect += $collected;
			$totalModal += $modal;

			$product->uncollected = $uncollected;
			$product->uncollectedAmount = $uncollectedAmount;
			$product->collected = $collected;
			$product->modal = $modal;
		}

		$this->excel->getActiveSheet()->SetCellValue('A1', "Product Name");
		$this->excel->getActiveSheet()->SetCellValue('B1', "Product capital price");
		$this->excel->getActiveSheet()->SetCellValue('C1', "Sell Quantity");
		$this->excel->getActiveSheet()->SetCellValue('D1', "Modal");
		$this->excel->getActiveSheet()->SetCellValue('E1', "Sell Price");
		$this->excel->getActiveSheet()->SetCellValue('F1', "Netto");

		$index = 2;
		foreach ($products as $product) {
			if ($product->uncollected > 0) {
				$this->excel->getActiveSheet()->SetCellValue('A'.$index, $product->product_title);
				$this->excel->getActiveSheet()->SetCellValue('B'.$index, $product->product_capital_price);
				$this->excel->getActiveSheet()->SetCellValue('C'.$index, $product->uncollected);
				$this->excel->getActiveSheet()->SetCellValue('D'.$index, $product->modal);
				$this->excel->getActiveSheet()->SetCellValue('E'.$index, $product->uncollectedAmount);
				$this->excel->getActiveSheet()->SetCellValue('F'.$index, $product->collected);
			
				$index++;
			}
		}
		
		$index += 2;

		$this->excel->getActiveSheet()->SetCellValue('D'.$index, "Total netto");
		$this->excel->getActiveSheet()->SetCellValue('E'.$index, $totalCollect);
		$index++;
		
		$this->excel->getActiveSheet()->SetCellValue('D'.$index, "Total modal");
		$this->excel->getActiveSheet()->SetCellValue('E'.$index, $totalModal);
		
		//$this->excel->save(APPPATH."xls/invoice2.xls");
		$this->excel->stream("soldItem".'.xls');
	
    }

    public function collectAll(){
    	$this->load->model("kenmodel");

    	$sells = $this->kenmodel->select("sell", "isCollected = 0", null, false);
    	foreach ($sells as $sell) {
    		$sell->isCollected = 1;
			$result = $this->kenmodel->addOrUpdate('sell', $sell);
    	}

    	$this->session->set_userdata('error',array('call'=>'success','info'=>'Collect money success'));
    	redirect(base_url().'admin/home');
    }
}