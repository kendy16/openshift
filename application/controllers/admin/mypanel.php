<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class mypanel extends MY_Controller {

	function __construct(){
		parent::__construct();
        $this->pageName = $this->projectName." - Dashboard";
    }

    public function index(){
        redirect(base_url().'admin/home');
    }
    
    public function panel($tableName){
        $this->headerNavigation = "$tableName";

        $this->pageName = "$tableName management";
        if(!$tableName) {
            redirect(base_url().'admin/home');
        }
        $this->load->model('kenmodel');
        //get data
        $data = $this->kenmodel->select($tableName);
        //field name etc
        $fields = $this->db->field_data($tableName);
        
        $deleteAble = true;
        $switchAble = true;
        if($tableName == "category" || $tableName == "category_header") {
            $deleteAble = false;
        } else if($tableName == "user") {
            $deleteAble = false;
            $switchAble = false;
        }

        $this->templateAdmin('admin/panel.php', array('data' => $data, 'fields' => $fields, 'tableName' => $tableName, 'deleteAble' => $deleteAble, 'switchAble' => $switchAble, 'actionAble' => 1));

    }

    public function panel_form_product($tableName, $relatedId) {
        $this->headerNavigation = "$tableName";
        $this->pageName = "insert $tableName";

        $this->load->model('kenmodel');
        //field name etc
        $fields = $this->db->field_data($tableName);
        $foreignKeys = array();
        foreach($fields as $field) {
            $fieldName = $field->name;
            if(strpos($fieldName, 'fk_') !== FALSE) {
                $fkTable = $fieldName;
                $fkTable = str_replace("fk_", "", $fkTable);
                $fkTable = str_replace("_id", "", $fkTable);
                $foreignKeys[$fieldName] = $this->kenmodel->select($fkTable, "product_id = $relatedId");
            }
        }

        $this->templateAdmin('admin/panel_form.php', array('data' => null, 'fields' => $fields, 'tableName' => $tableName, 'foreignKeys' => $foreignKeys));   
    }

    public function panel_form($tableName, $id=null, $copy=false){
        $this->headerNavigation = "$tableName";
		
		if($id == null) {
			$this->pageName = "insert $tableName";
		} else {
			$this->pageName = "update $tableName";
		}
        if(!$tableName) {
            redirect(base_url().'admin/home');
        }
        $this->load->model('kenmodel');
        //get data
        $data = $this->kenmodel->selectById($tableName, $id);
        if($copy) {
            $idName = $tableName."_id";
            $data->$idName = null;
            $this->pageName = "Copy $tableName";
        }
        //field name etc
        $fields = $this->db->field_data($tableName);
        $foreignKeys = array();
        foreach($fields as $field) {
            $fieldName = $field->name;
            if(strpos($fieldName, 'fk_') !== FALSE) {
                $fkTable = $fieldName;
                $fkTable = str_replace("fk_", "", $fkTable);
                $fkTable = str_replace("_id", "", $fkTable);
                $foreignKeys[$fieldName] = $this->kenmodel->select($fkTable);
            }
        }

        $this->templateAdmin('admin/panel_form.php', array('data' => $data, 'fields' => $fields, 'tableName' => $tableName, 'foreignKeys' => $foreignKeys));
    }
	
	public function addOrEdit($tableName) {
		$data = array();
		foreach($this->input->post() as $key => $val)
		{
			$data[$key] = $val;
			//echo "<p>Key: ".$key. " Value:" . $val . "</p>\n";
		}
		//echo print_r($data);

        //manipulate data
        $fields = $this->db->field_data($tableName);
        foreach ($fields as $field) {
            $fieldName = $field->name;
            if($field->type == "varchar" && strpos($fieldName, 'password') !== FALSE){
                //md5 for password
                $data[$fieldName] = md5($data[$fieldName]);
            }
        }

		$this->load->model("kenmodel");
		$result = $this->kenmodel->addOrUpdate($tableName, $data);
        $this->session->set_userdata('error',array('call'=>'success','info'=>$result));

        if ($tableName == "buy" || $tableName == "sell") {
            redirect(base_url()."admin/home");    
        } else IF ($tableName == "product") {
			redirect(base_url()."admin/mypanel/panel_form/product");
		} else{
		    redirect(base_url()."admin/mypanel/panel/$tableName");
        }
	}

    public function bringUpOrDown ($tableName, $id, $down = false) {
        $this->load->model("kenmodel");
        $fieldId = $tableName."_id";
        $data = $this->kenmodel->selectById($tableName, $id, false);

        $where = $down ? array(
            $fieldId.' >' => $id
        ): array(
            $fieldId.' <' => $id
        );
        $this->db->limit(1);
        if(!$down) {
            $this->db->order_by("$fieldId desc");
        }
        $otherData = $this->kenmodel->select($tableName, $where, null, false);
        if(count($otherData) > 0) {
            $otherData= $otherData[0];
            $dataId = $data->$fieldId;
            $otherDataId = $otherData->$fieldId;
            $tempDataId = 0;

            $otherData->$fieldId = $tempDataId;
            $result = $this->kenmodel->updateById($tableName, (array)$otherData, $otherDataId);

            $data->$fieldId = $otherDataId;
            $result = $this->kenmodel->updateById($tableName, (array)$data, $dataId);

            $otherData->$fieldId = $dataId;
            $result = $this->kenmodel->updateById($tableName, (array)$otherData, $tempDataId);


            $this->session->set_userdata('error',array('call'=>'success','info'=>$result));
        } else {
            $topOrBottom = $down ? "Bottom" : "Top";
            $this->session->set_userdata('error',array('call'=>'error','info'=>"You reach the $topOrBottom limit."));
        }
        redirect(base_url()."admin/mypanel/panel/$tableName");
    }

    public function quickChange($tableName, $id, $field, $value) {
        $this->load->model("kenmodel");
        $data = $this->kenmodel->selectById($tableName, $id, false);
        $data->$field = $value;
        $result = $this->kenmodel->addOrUpdate($tableName, (array)$data);
        $this->session->set_userdata('error',array('call'=>'success','info'=>$result));
        redirect(base_url()."admin/mypanel/panel/$tableName");
    }

    public function sendMail() {
        $toMail = $this->input->post('email');
        $subject = $this->input->post('subject');

        $message = '<html><body>';
        $message .= $this->input->post('message');
        $message .= '<html><body>';

        $email = "hello@nini-store.com";
        $error = $this->pub->doSendMail($toMail, $subject, $message, $email);
        $this->session->set_userdata('error',$error);
        redirect(base_url().'admin');
    }

    public function backup(){
        //todo failed backup
        $backupName = $this->companyName." - ".date('Y-M-d').'.gz';
        // Load the DB utility class
        $this->load->dbutil();

        // Backup your entire database and assign it to a variable
        $backup =& $this->dbutil->backup(); 

        // Load the file helper and write the file to your server
        $this->load->helper('file');
        write_file(base_url().'database/'.$backupName, $backup); 

        // Load the download helper and send the file to your desktop
        $this->load->helper('download');
        force_download($backupName, $backup);
    }
}