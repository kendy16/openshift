<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class login extends MY_Controller {
    function __construct(){
		parent::__construct();
        $this->headerVisible = false;
        $this->footerVisible = true;
        $this->headerSliderVisible = false;
        $this->pageName = $this->projectName." - Admin Login";
    }
    public function index()
    {
        $this->load->library('parser');
        if(!$this->userLogin || $this->userLogin['title'] != $this->keySession){
            $this->template('admin/login.php', 
                        array("companyName" => $this->companyName, "copyright" => $this->copyright, "kendyEmail" => $this->kendyEmail, "kendyGroup" => $this->kendyGroup),
                        array("admin/admin_overlay_error.css"));
        }else {
			redirect(base_url()."admin/home");
        }
    }

    public function doLogin(){
        session_start();    
        $username = $this->input->post('username');
        $password = $this->input->post('password');

        $this->load->model('kenmodel');
        $user = $this->kenmodel->select('user', 
            array(
                'user_username'=>$username,
                'user_password'=>md5($password)
            ));

        if(empty($username) || empty($password)){
            $info = 'username and password must be filled';
        }else if(count($user)==1){
            $user = $user[0];
			$loginData = array(
				'id'=>$user->id,
				'username'=>$username,
				'title'=>$this->keySession,
				'loggedIn'=>true
			);
			$_SESSION['userLogin']=$loginData;
            $this->session->set_userdata('userLogin',$loginData);
                        
            $error=array(
                'call'=>'success',
                'info'=>'Welcome, '.$username.'.. You are in admin interface now!'
            );
            $this->session->set_userdata('error',$error);
            
            redirect(base_url().'admin/home');
            return;
        }else{
            $info = 'incorrect username or password';
        }

        $this->session->set_userdata('error',array('call'=>'error','info'=>$info));
        redirect(base_url().'admin/login');
    }

    public function doLogout(){
        $this->session->unset_userdata('userLogin');
        //$this->session->sess_destroy();
		//session_start();
		//session_destroy();
        //todo : not working 
                        
        $error=array(
            'call'=>'success',
            'info'=>'Thank you, You are logged out!'
        );
        $this->session->set_userdata('error',$error);

        redirect(base_url().'admin/login');
    }
}