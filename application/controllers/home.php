<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {
	function __construct(){
		parent::__construct();
        $this->pageName = $this->companyName." - Home";
        $this->navigationName = "Home";
	}
    public function index()
    {
        redirect(base_url()."admin/login");
    }
}
