<div class="container">

	<!--Product package-->
	<div class = "product-package">
		<?php foreach ($products as $product) { ?>
			<div class = "product-item col-sm-3">
				<div class = "product-pack">
					<div class = "product-img">
						<?php 
							$imageUrl =  $product->product_image;
							$imageUrlThumbnail = str_replace("images", "_thumbs/Images", $imageUrl);
						?>
						<img class="lazy" src="<?php echo $imageUrlThumbnail;?>" data-original="<?php echo $imageUrl;?>" alt="nini-store <?php echo $product->product_name;?>">
						<?php if($product->product_hot_item == 1) { ?>
							<img class="hotIcon" src="<?php echo base_url()."img/web/hot-item.png"?>" alt="nini-store hot item icon">
						<?php }
						if($product->product_quantity <= 0) { ?>
							<img class="soldOutIcon" src="<?php echo base_url()."img/web/sold-out-item.png"?>" alt="nini-store sold out icon">
						<?php }
						if($product->product_preorder == 1) { ?>
							<img class="preorderIcon" src="<?php echo base_url()."img/web/preorder.png"?>" alt="nini-store preorder icon">
						<?php }
						?>
					</div>
					<div class = "product-info">
						<h1><?php echo $product->product_name;?></h3>
						<?php if($product->product_display_price > 0) { ?>
							<h2>IDR <?php echo $this->converter->formatNumber($product->product_display_price);?></h1>
						<?php }?>
						<h3>IDR <?php echo $this->converter->formatNumber($product->product_price);?></h2>
					</div>
					<div class = "product-detail">
						<div class = "product-size">
							<?php
							foreach (explode(",",$product->product_size) as $size) {
								echo "<b>$size</b>";
							}
							?>
						</div>

						<?php if(!empty($product->product_material)) { ?>
							<div class = "product-description">
								<span>
								<?php echo $product->product_material;?>
								</span>
							</div>
						<?php }?>
						<div class = "product-description">
							<?php echo $product->product_description;?>
							<?php echo $productDescription;?>
						</div>
					</div>
				</div>
			</div>
		<?php }?>
	</div>

	<!-- PRODUCT Modal-->
	<div id="productModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div class="modal-content">
	      <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">Product Detail</h4>
	      </div>
	      <div style="text-align:center;padding-top:5px;margin-bottom:-15px;">
	      	Tekan arrow "<" untuk melihat product sebelumnya atau ">" untuk melihat product setelahnya...
	  	  </div>
	      <div class="modal-body">
			Empty.
	      </div>
	    </div>
	  </div>
	</div>
</div>