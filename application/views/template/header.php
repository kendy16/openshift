<!-- Nav bar -->
<div class="navbar-wrapper">
	<div class="container">
	<!-- navbar type : navbar-default, navbar-inverse    navbar-fixed-top[if there is no right navbar], navbar-static-top -->
		<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
		  <div class="container">
			<div class="navbar-header">
			  	<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
			  	</button>
			  	<a class="navbar-brand" href="<?php echo base_url();?>">
			  		<div id="logo-temp">
				  		<img height="100px" src="<?php echo base_url().'img/ckfinder/files/logo.png';?>" alt="nini-store logo icon"/>
				  	</div>
			  	</a>
			</div>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
				<?php 
				foreach($navigations as $nav) {
					$class = "";
					$activeNav = explode(",",$navigationName);
					$headerActive = $activeNav[0];
					@$childActive = $activeNav[1];
					if(@count($nav["child"]) > 0) {
						$headerClass = "";
						if($headerActive == $nav["title"]) $headerClass = "active";
						
						echo '
						<li class="dropdown '.$headerClass.'">
						  <a href="#" class="dropdown-toggle" data-toggle="dropdown">'.$nav['title'].'<b class="caret"></b></a>
						  <ul class="dropdown-menu">';
							$i = count($nav['child']);
							foreach($nav['child'] as $child) {
								echo '<li class="dropdown-header">'.$child["category"].'</li>';
								foreach(@$child["subcategory"] as $each) {											
									$childClass = "";
									if($childActive == $each["title"]) $childClass = "active";
									echo '<li class="'.$childClass.'"><a href="'.@$each['action'].'">'.$each['title'].'</a></li>';
								}
								if($i != 1 ) {echo '<li class="divider"></li>'; $i--;};
							}
						echo '
						  </ul>
						</li>';
					} else {
						$class = "";
						if($nav["title"] == $headerActive) $class = "active";
						echo '<li class="'.$class.'"><a href="'.@$nav["action"].'">'.$nav["title"].'</a></li>';
					}
				}
				?>
				
				<!-- Base template
				<li><a href="<?php echo base_url();?>">Home</a></li>
				<li class="dropdown active">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown">Product <b class="caret"></b></a>
				  <ul class="dropdown-menu">
					<li class="dropdown-header">Fashion</li>
					<li class="active"><a href="<?php echo base_url().'product'; ?>">T-Shirt</a></li>
					<li class="divider"></li>
					<li class="dropdown-header">Accesories</li>
					<li><a href="#">Coming soon</a></li>
				  </ul>
				</li>
				<li><a href="<?php echo base_url().'HowToOrder'; ?>">Cara order</a></li>
				<li><a href="">Card : <span class="badge">0 Items</span></a></li>
				<li><a href="#about">Why Us</a></li>
				-->
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li></li>
				<!--must have an empty navbar right -->
				<li><a href=""></a></li>
			  </ul>
			  <!--
			  <form class="navbar-form navbar-right">
				<input type="search" class="form-control" placeholder="i.e : Shoes, local brand,.." required>
				<button class="btn btn-default" type="submit">Find!</button>
				<a href="" onclick="return false;" data-toggle="modal" data-target="#loginModal" class="btn btn-success">Login</a>
				<a href="" onclick="return false;" data-toggle="modal" data-target="#signupModal" class="btn btn-primary">Sign up!</a>
			  </form>
			  -->
			</div>
		  </div>
		</div>
	</div>
</div>
	
	<!-- End of nav bar -->

<!-- LOGIN modal -->
<div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Please sign in</h4>
      </div>
      <div class="modal-body">
        <form role="form">
            <div class="form-group">
			     <input type="email" class="form-control" placeholder="Email address" required autofocus>
            </div>
            <div class="form-group">
			     <input type="password" class="form-control" placeholder="Password" required>
            </div>
        
			<label class="checkbox">
			  <input type="checkbox" value="remember-me"> Remember me
			</label>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
		</form>
      </div>
	  <!--
      <div class="modal-header">
        <h4 class="modal-title" id="myModalLabel">Login</h4>
      </div>
      <div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
	  -->
    </div>
  </div>
</div>
<!-- End login modal -->

<!-- SIGNUP modal -->
<div id="signupModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Sign Up</h4>
      </div>
      <div class="modal-body">
        <form role="form">
            <div class="form-group">
                <!--<label class="control-label" for="emailsu">Input with success</label>-->
                <input id="emailsu" type="email" class="form-control" placeholder="Email address" required autofocus>
            </div>
            <div class="form-group">
			     <input type="password" class="form-control" placeholder="Password" required>
            </div>
            <div class="form-group">
			     <input type="password" class="form-control" placeholder="Re-password" required>
            </div>
            <div class="form-group">
			     <textarea class="form-control" placeholder="address" required style="resize:none"></textarea>
            </div>
            <div class="form-group">
			     <input type="tel" class="form-control" placeholder="Telelphone" required>
            </div>
            <div class="form-group">
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign Up</button>
                <button type="button" class="btn btn-lg btn-warning btn-block" data-dismiss="modal">Close</button>
            </div>
		</form>
      </div>
    </div>
  </div>
</div>
<!-- End SIGNUP modal -->
