<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
    <head>
        <title>{title}</title>
		<link rel="icon" href='<?php echo base_url();?>img/ckfinder/files/icon.png' type='image/x-icon'>
        <link rel="shortcut icon" href='<?php echo base_url();?>img/ckfinder/files/icon.png'>
		        
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="keywords" content="{metaDat}" />
		<meta name="description" content="{metaDesc}" />
        <!--<meta name='subject' content='your websites subject'>-->
        <meta name='copyright' content='nini-store'>
        <meta name="author" content="{metaAuthor}">
		<meta name="language" content="indonesia" />
        <meta name="organization" content="nini-store" />
        <meta name="audience" content="Viewer of nini-store" />
		<meta name="robots" content="index,follow" />
		<meta name="revisit-after" content="1 days" />
        <meta name="classification" content="Indonesia, online shop, tas, parfum, kosmetik" />
        <meta name="rating" content="general" />
        
        
        
        <!-- Bootstrap core CSS -->
        <link href="<?php echo base_url(); ?>css/bootstrap.css" rel="stylesheet">

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Custom styles for this template -->
        <link href="<?php echo base_url(); ?>css/carousel.css" rel="stylesheet">
        
        <!-- Additional style css -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css">
	
        {css}
        
        <script type="text/javascript" src="<?php echo base_url(); ?>js/jquery-1.9.1.mis.js"></script>
    </head>
    <body>
	    {header}
        {headerSlider}
        <div class="container" id="content-body">
            {content}
        </div>
        {footer}
        {error}
        {setting}
        
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script type="text/javascript" src="<?php echo base_url(); ?>js/web/pub.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>js/docs.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.lazyload.js"></script>
    <script src="<?php echo base_url(); ?>js/overlay-error.js"></script>
    {js}
        
    </body>
</html>