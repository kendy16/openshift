<!-- FOOTER
<footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p>&copy; 2014 Company, Inc. &middot; <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer> -->
<style>
    .additionalButton {
        bottom:70px;
        left:-20px;
    }.additionalButton div {
        margin-top:5px;
    }.additionalButton a,.additionalButton span {
        font-size:12px;
        line-height: 10px;
        padding-left:30px;
    }

    #live-chat {
        width:300px;
        height:400px;
        background: rgba(0,0,0 ,0.8);
        margin-left:200px;
        position: fixed;
        bottom:0px;
        z-index: 9999;
        display: none;
    }#live-chat #live-chat-button {
        display: inline-block;
        width: 100%;
    }#live-chat #live-chat-button b {
        padding: 5px;
        color:white;
        font-size: 20px;
    }#live-chat #live-chat-button button {
        float:right;
        margin:5px;
    }#live-chat #live-chat-header {
        background: white;
        padding: 10px;
        border: 3px solid rgba(0,0,0, 0.9);
    }#live-chat #live-chat-content {
        background: white;
        padding: 10px;
        width:100%;
        height: 180px;
        text-align: left;
        margin-bottom:-10px;
    }#live-chat #live-chat-attention {
        color:white;
        width:100%;
        height:70px;
        padding:15px;
        font-size:12px;
        margin:0px;
    }#live-chat #live-chat-message-field {
        border: 3px solid rgba(0,0,0, 0.9);
        width:100%;
        height:60px;
        padding:15px;
    }
@media (max-width: 500px) {
}
</style>

<div class="footer">
    <div class="container" style="position:relative; padding-top:10px;">
        <p class="text-muted">&copy; <?php echo $copyright." ".$companyName;?> - Supported by <a href="<?php echo $kendyEmail;?>"><?php echo $kendyGroup;?></a></br>
        <?php
            foreach ($footerLinks as $link)
            {
                echo '<a href="'.$link->social_link_value.'" style="margin-right:10px;">&middot; '.$link->social_link_title.'</a>';
            }
            ?>
        </p>
        <a href="#" style="position:absolute; right:10px; top:10px;">Back to Top</a>
    </div>
</div>