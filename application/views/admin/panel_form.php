<a href="<?php echo base_url()."admin/mypanel/panel/$tableName"; ?>" class="btn btn-success capital">Back to <?php echo $tableName; ?> list</a>

<div class=".col-sm-" style="margin-bottom:20px;">
<form id="insertForm" role="form" method="post" action="<?php echo base_url()."admin/mypanel/addOrEdit/$tableName";?>" >
<?php
foreach ($fields as $field) {
	echo "<div class='form-group'>";
	$fieldName = $field->name;
	$value = @$data->$fieldName;
	if($field->primary_key == 1) {
		echo "
			<input type='hidden' name='$fieldName' value='$value' />
		";
	} else if ($field->type == "int" && strpos($fieldName, 'fk_') !== FALSE){
		//for foreign key
		echo "<select class='form-control' name='$fieldName'>";
		foreach ($foreignKeys[$fieldName] as $fkObject) {
			$optionValue = str_replace("fk_", "", $fieldName);
			$getTitle = str_replace("_id", "", $optionValue);
			$getTitle = $getTitle.'_title';
			$option = $fkObject->$getTitle;
			$optionValue = $fkObject->$optionValue;
			$selected = "";
			if($optionValue == $value) $selected = "selected";
			echo "<option value='$optionValue' $selected>$option</option>";
		}
		echo "</select>";
	} else {
		if ($field->type == "tinyint") {
			//boolean
			$truefalse = @$data->$fieldName == 1 ? "checked" : "";
			$option = str_replace($tableName."_", "", $fieldName);
			$option = str_replace("_", " ", $option);
			echo "
				<label class='radio-inline capital'><input type='radio' style='margin-top:0px;' name='$fieldName' value='0' checked>Not $option</label>
				<label class='radio-inline capital'><input type='radio' style='margin-top:0px;' name='$fieldName' value='1' $truefalse>$option</label>
				";
		} else if ($field->type == "int") {
			//int
			echo "<input type='text' class='form-control integerOnly' placeholder='$fieldName' required autofocus name='$fieldName' value='$value'>";
		} else if ($field->type == "double" || $field->type == "float") {
			//double/float
			echo "<input type='text' class='form-control floatOnly' placeholder='$fieldName' required autofocus name='$fieldName' value='$value'>";
		} else if ($field->type == "date") {
			//date
			echo "<input type='text' class='form-control dPicker' placeholder='$fieldName' required autofocus name='$fieldName' value='$value'>";
		} else if ($field->type == "text") {
			//textarea
			echo "<textarea name='$fieldName' class='ckeditor'>$value</textarea>";
		} else if ($field->type == "varchar" && strpos($fieldName, 'image') !== FALSE){
			//image varchar
			if($value == "" || $value == NULL) $value = base_url().'img/web/upload-image.png';
			echo "
				<img id='imagePreview' src='$value' height='50px' alt='Image Preview' onclick='BrowseServer();' style='cursor:pointer;margin:10px;border:1px solid black;'/>
				<p>
					Selected Image URL :
					<input id='imageBrowser' name='$fieldName' type='text' size='60' required value='$value' readonly/>
				</p>
			";
		} else if ($field->type == "varchar" && (strpos($fieldName, 'password') !== FALSE || strpos($fieldName, 'secure') !== FALSE)){
			//secure
			echo "<input type='password' class='form-control' placeholder='$fieldName' required autofocus name='$fieldName' value='$value'>";
		} else {
			//standard varchar
			echo "<input type='text' class='form-control' placeholder='$fieldName' required autofocus name='$fieldName' value='$value'>";
		}
	}
	echo "</div>";
}
?>
<button class="btn btn-lg btn-primary btn-block" type="submit">SAVE</button>
</form>
</div>