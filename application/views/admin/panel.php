<style>
.button-pack {
	width: 100px;
}
.button-pack a{
	margin-top:5px;
}
blockquote {
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
}
blockquote:before {
  color: #ccc;
  content: "\201C Script and Style";
  font-size: 25px;
  line-height: 0.1em;
  margin-right: 0.25em;
}
blockquote p {
  display: inline;
}
</style>
<?php
if ($tableName != "dashboard") {
?>
<a href="<?php echo base_url()."admin/mypanel/panel_form/$tableName"; ?>" class="btn btn-success capital">Insert <?php echo $tableName; ?></a>
<?php } else {
	?>
<a href="<?php echo base_url()."admin/home/downloadSoldItem"; ?>" class="btn btn-warning capital">Download Sold Item</a>
<a href="<?php echo base_url()."admin/home/collectAll"; ?>" class="btn btn-success capital">Collect Rp. <?php echo $this->converter->formatNumber($totalCollect); ?></a>
<?php }?>
<div class="row">
	<section class="main column groupBox">
	    <article class="module width_3_quarter">
	        <header>
	            <h3 class="tabs_involved">
	                <div class="font_title capital"><?php echo $tableName; ?> List</div>
	            </h3>
	        </header>
	        <table cellspacing="0" class="dtTables">
	            <thead>
	            <tr>
					<?php
					foreach ($fields as $field) {
						$fieldName = $field->name;
						if ($field->type == "int" && strpos($fieldName, 'fk_') !== FALSE){
							$fieldName = str_replace("fk_", "", $fieldName);
							$fieldName = str_replace("_id", "", $fieldName);
						}
						$fieldName = str_replace($tableName."_", "", $fieldName);
						$fieldName = str_replace("_", " ", $fieldName);

						$width="";
						if ($field->type == "int"){
							$width = "width='20px'";
						} else if ($field->type == "int" && strpos($fieldName, 'fk_') !== FALSE){
						} else {
							if ($field->type == "tinyint") {
								$width = "width='100px;'";
							} else if ($field->type == "varchar" && (strpos($fieldName, 'password') !== FALSE || strpos($fieldName, 'secure') !== FALSE)){
							} else if($field->type == "varchar" && strpos($fieldName, 'image') !== FALSE) {	
								$width = "width='100px'";
							} else if($field->type == "text"){
								$width = "width='200px'";
							} else if($field->type == "custom") {
								$width = "width='100px'";
							}
						} 
						echo "<td class='capital' style='text-align:center;' $width>".$fieldName."</td>";
					}
					
					if ($tableName == "sell") {
						echo "<td>Untung</td>";
					}

						if(@$isModal) {
							?>
					<td width="100px">MODAL</td>
							<?php
						}

						if($actionAble) {
							?>
					<td width="100px">Action</td>
							<?php
						}
					?>
				</tr>
	            </thead>
	            
	            <tbody>
				<?php
				foreach ($data as $dat) { 
					$fieldName = $fields[0]->name;
					$dat_id = $dat->$fieldName; ?>
				<tr>
					<?php
					foreach ($fields as $field) {
						$fieldName = $field->name;
						if ($field->type == "int" && strpos($fieldName, 'fk_') !== FALSE){
							//for foreign key
							$fieldName = str_replace("fk_", "", $fieldName);
							$getTitle = str_replace("_id", "", $fieldName);
							$getTitle = $getTitle.'_title';
							echo "<td>".$dat->$getTitle."</td>";
						} else {
							if ($field->type == "tinyint") {
								//boolean
								$href = base_url()."admin/mypanel/quickChange/$tableName/".$dat_id."/".$field->name."/".!$dat->$fieldName;
								$truefalse = $dat->$fieldName == 1 ? "<a href='$href' class='btn btn-sm btn-success noLine' ><span class='glyphicon glyphicon-ok'></span> Active</a>" 
																	: "<a href='$href' class='btn btn-sm btn-danger noLine' ><span class='glyphicon glyphicon-minus'></span> NO</a>";
								echo "<td style='text-align:center;'>".$truefalse."</td>";
							} else if ($field->type == "text") {
								//textarea
								if (strpos($dat->$fieldName, '</script>') !== FALSE || strpos($dat->$fieldName, '</style>') !== FALSE) {
									//echo "<td width='1200px'><textarea style='width:100%;height:100%;'>".$dat->$fieldName."</textarea></td>";
									$script = "</br>".$dat->$fieldName;
									$script = str_replace("{\n}", "{</br>}", $script);
									$script = str_replace("{\n", "{</br>&nbsp;&nbsp;&nbsp;&nbsp;", $script);
									$script = str_replace(">.", "></br>.", $script);
									$script = str_replace(">\n", "></br>", $script);
									$script = str_replace(";\n}", ";</br>}", $script);
									$script = str_replace(";\n<", ";</br><", $script);
									$script = str_replace(";\n", ";</br>&nbsp;&nbsp;&nbsp;&nbsp;", $script);
									$script = str_replace("\n", "</br>", $script);
									$script = str_replace("<script", "< script", $script);
									$script = str_replace("/script>", " /script >", $script);
									$script = str_replace("<style", "< style", $script);
									$script = str_replace("/style>", " /style >", $script);
									$script = str_replace("fixed", "FIXEDD", $script);
									echo "<td width='1200px'><div style='overflow-y:scroll;overflow-x:hidden;height:200px;'>
									<blockquote>
									".$script."
									<p></p></blockquote>
									</div></td>
									";
								} else {
									$script = str_replace("fixed", "FIXED", $dat->$fieldName);
									echo "<td width='300px' height='100px'><div style='overflow-y:scroll;overflow-x:hidden;height:200px;width:300px;'>".$script."</div></td>";
								}
							} else if ($field->type == "varchar" && strpos($fieldName, 'email') !== FALSE){
								//email
								echo "<td><a href='' onclick='return false;' class='email'>".$dat->$fieldName."</td>";
							} else if ($field->type == "varchar" && (strpos($fieldName, 'password') !== FALSE || strpos($fieldName, 'secure') !== FALSE)){
								//secure
								echo "<td>".$dat->$fieldName."</td>";
							} else if($field->type == "varchar" && strpos($fieldName, 'image') !== FALSE) {
								//image
								$imgUrl = $dat->$fieldName;
								$loadingGif = base_url()."img/web/loading.gif";
								echo "
									<td style='text-align:center;'><img class='lazy' src='$loadingGif' data-original='$imgUrl' height='250px' /></td>
								";
							} else if (($field->type == "int" || $field->type == "double") && (strpos($fieldName, '_id') === false) ) {
								echo "<td>".$this->converter->formatNumber($dat->$fieldName)."</td>";
							} else if ($field->type == "custom") {
								?>
								<td class="button-pack" style='text-align:center; max-width:100px;width:100px;min-width:100px;'>
		                		<a href="<?php echo base_url()."admin/mypanel/panel_form_product/buy/$dat_id"; ?>" class="btn btn-success capital btn-sm noLine">Buy</a>
		                		<a href="<?php echo base_url()."admin/mypanel/panel_form_product/sell/$dat_id"; ?>" class="btn btn-warning capital btn-sm noLine">Sell</a>
								</td>

								<?php
							} else {
								echo "<td>".$dat->$fieldName."</td>";
							}
						} 
					}

					if ($tableName == "sell") {
						$untung = $dat->sell_price - ($dat->product_capital_price * $dat->sell_quantity);
						$untung = max($untung, 0);
						echo "<td>".$this->converter->formatNumber($untung)."</td>";
					}

					if (@$isModal) {
						echo "<td>".$this->converter->formatNumber($dat->product_capital_price)."</td>";
					}

						if($actionAble) {
							?>
					<td class="button-pack" style='text-align:center; max-width:100px;width:100px;min-width:100px;'>
	                	<?php 
	                		$this->pub->updateAction(base_url()."admin/mypanel/panel_form/$tableName/$dat_id");
	                		$this->pub->copyAction(base_url()."admin/mypanel/panel_form/$tableName/$dat_id/true");
	                		if($switchAble) {
		                		$this->pub->upAction(base_url()."admin/mypanel/bringUpOrDown/$tableName/$dat_id");
		                		$this->pub->downAction(base_url()."admin/mypanel/bringUpOrDown/$tableName/$dat_id/1");
	                		}
	                		if($deleteAble) {
	                			$this->pub->deleteAction(base_url()."admin/home/deleteRecord/$tableName/$dat_id/$tableName", "Are you sure want to delete '$tableName' with Id '$dat_id' ?");
	                		}
	                	?>
					</td>
							<?php
						}
					?>
				</tr>
				<?php }	?>	
	            </tbody>
	        </table>
	    </article>
	</section>
</div>