<script>
var thistimeout;
function updateMessage(timeinterval){
    clearTimeout(thistimeout);
    thistimeout = setTimeout(function () {
        $.get( baseUrl + "livechat/getAllLiveChatActive/", function( data ) {
            $content = $(".live-chat-pack");
            $content.html(data);
            $(".live-chat-content").scrollTop($(".live-chat-content")[0].scrollHeight);
        });
        updateMessage(2000);
    }, timeinterval);	
}

$( window ).keypress(function() {
	clearTimeout(thistimeout);
    updateMessage(10000);
});

function closeLiveChat(id) {
	clearTimeout(thistimeout);
    $.get( baseUrl + "livechat/closeLiveChatFromAdmin/" + id, function( data ) {
        $content = $(".live-chat-pack");
        $content.html(data);
        $(".live-chat-content").scrollTop($(".live-chat-content")[0].scrollHeight);
    });
    updateMessage(2000);
}

function sendMessage(id) {
	if($("#live-chat-id-" + id).find(".live-chat-message").val() == "") return false;
    var message = $("#live-chat-id-" + id).find(".live-chat-message").val() + "\n";
    message = "nini-store : " + message;
    $.post( baseUrl + "livechat/addMessageFromAdmin/" + id, {
        message : message
    }).done(function( data ) {
        $content = $("#live-chat-content-"+id);
        $content.html(data);
        $content.scrollTop($content[0].scrollHeight);
        $("#live-chat-id-" + id).find(".live-chat-message").val("");
    });        
    updateMessage(2000);

	return false;
}

updateMessage(2000);
</script>

<style>
.live-chat-pack {
	overflow: hidden;
}
.live-chat-item {
	border:1px solid black;
	background: rgba(0,0,0, 0.5);
	padding:20px;
	float:left;
	height:400px;
	width:30%;
	position: relative;
	margin-bottom: 20px;
	margin-left:20px;
}
.live-chat-item p, .live-chat-item textarea, .live-chat-item button, .live-chat-item input {
	width: 100%;
	color:white;
	font-size: 18px;
}
.live-chat-item .live-chat-content {
	height:220px;
	color:black;
	font-size:12px;
	padding: 5px;
	border: 1px solid black;
}
.live-chat-item .live-chat-message {
	color:black;
	font-size:12px;
	padding: 5px;
	border: 1px solid black;
}
.live-chat-item button {
	bottom:0px;
	position: absolute;
	margin-left:-20px;
	border-top: 1px solid black;
}
</style>


<a href="<?php echo base_url()."livechat/removeAllClosedLiveChat"; ?>" class="btn btn-danger capital">Remove all closed chat</a>
<a href="<?php echo base_url()."livechat/turnOnOff/0"; ?>" class="btn btn-warning capital">Turn off</a>
<a href="<?php echo base_url()."livechat/turnOnOff/1"; ?>" class="btn btn-success capital">Turn on</a>
</br></br>
<div class="live-chat-pack">
	<h1>Waiting...</h1>
</div>