<script>
$(document).ready(function(){
  $("#menu-button").click(function() {
    $side = $(".sidebar");
    if($side.css("display") == "none") {
      $side.css("display","block");
      $('html, body').animate({ scrollTop: 0 }, 'fast');
    } else {
      $side.css("display","none");
    }
  });
});
</script>
<a class='btn btn-sm btn-success noLine' id='menu-button'><span class='glyphicon glyphicon-th-large'></span> Menu</a>
<div class="col-sm-3 col-md-2 sidebar">
  <ul class="nav nav-sidebar">
    <li><a href="<?php echo $projectLink;?>">Welcome, <?php echo $username; ?></a></li>
    <!-- <li><a href="#" onclick='BrowseServer();'>Image Management</a></li> -->
  </ul>
  <ul class="nav nav-sidebar">
    <?php
    foreach($navigations as $nav) {
        $class = "";
        if($nav == $active_navigation) $class = "active";
        $navText = str_replace("_", " ", $nav);
        echo '<li class="'.$class.'"><a href="'.base_url().'admin/mypanel/panel/'.$nav.'" style="text-transform: capitalize;">'.$navText.'</a></li>';
    }
    ?>
  </ul>
</div>