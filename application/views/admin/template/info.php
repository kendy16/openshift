<div id="eContainer">
    <button type="button" class="close eClose">×</button>
    <?php
    //alert-success;alert-info;alert-waring;alert-danger;
    $class = "alert-success";
    if($call == "error"){
        $class = "alert-danger";
    }
    ?>
    <div class="alert <?php echo $class;?>" role="alert" style="display:inline-block;">
        <strong style="text-transform:uppercase;"><?php echo $call;?> | </strong> <?php echo $info ; ?>
    </div>
</div>