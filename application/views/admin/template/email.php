<!--========Email Form=========-->
<script>
$(document).ready(function(){
  $(".email").click(function(){
      if($(this).is("a")) {
          $email = $(this).html();
          $("#email-field").val($email);
          $("#email-field").attr('readonly', true);
      }
      $("#emailModal").modal('show');
  });
});
</script>
<!-- Email modal -->
<div id="emailModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Send Email</h4>
      </div>
      <div class="modal-body">
        <form role="form" method="post" action="<?php echo base_url();?>admin/mypanel/sendMail">
            <div class="form-group">
                 <input type="text" class="form-control" placeholder="email" required autofocus name="email" id="email-field">
            </div>
            <div class="form-group">
                 <input type="text" class="form-control" placeholder="subject" required name="subject">
            </div>
            <div class="form-group">
                <textarea name='message' class='ckeditor'></textarea>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- End email modal -->
<!--======End Email Form=======-->