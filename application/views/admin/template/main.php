<!DOCTYPE html>
<html lang="en">
  <head>
    <title>{title}</title>
    <link rel="icon" href='<?php echo base_url();?>img/ckfinder/files/icon.png' type='image/x-icon'>
    <link rel="shortcut icon" href='<?php echo base_url();?>img/ckfinder/files/icon.png'>
      
    <meta name="robots" content="NOINDEX,NOFOLLOW" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url(); ?>/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?php echo base_url(); ?>/css/admin/dashboard.css" rel="stylesheet">
      
    <link href="<?php echo base_url(); ?>/css/admin/admin_overlay_error.css" rel="stylesheet">
      
    <!-- Additional style css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/main.css">    
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin/dataTables/demo-table.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/admin/dataTables/jquery-ui-1.8.23.custom.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery-ui.css"/>

    {css}

    <script src="<?php echo base_url(); ?>js/jquery-1.9.1.mis.js"></script>
  </head>

  <body>
    {header}
    <div class="container-fluid">
      <div class="row">
        {leftNav}
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main" style="margin-top:40px;">
            <h1 class="page-header capital">{title}</h1>
            {content}
            {footer}
        </div>
      </div>
    </div>
    {error}
    {emailForm}
    {setting}

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?php echo base_url(); ?>js/admin/dataTables/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>js/admin/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url(); ?>js/admin/ckfinder/ckfinder.js"></script>
    <script src="<?php echo base_url(); ?>js/admin/admin_pub.js"></script>
    
    <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>js/docs.min.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery-ui.js"></script>
    <script src="<?php echo base_url(); ?>js/jquery.lazyload.js"></script>
    <script src="<?php echo base_url(); ?>js/overlay-error.js"></script>

    {js}
  </body>
</html>
