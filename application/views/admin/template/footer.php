<footer>
    <p class="text-muted">&copy; <?php echo $copyright." ".$companyName;?> - Supported by <a href="<?php echo $kendyEmail;?>"><?php echo $kendyGroup;?></a></br>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <?php
    foreach ($footerLinks as $link)
    {
        echo '<a href="'.$link->social_link_value.'" style="margin-right:10px;">&middot; '.$link->social_link_title.'</a>';
    }
    ?>
</footer>