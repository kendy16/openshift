<script>
$(document).ready(function() {
    $('#loginModal').modal('show');
});
</script>

<div class="row" style="
margin:0px auto;
margin-top:20%;
text-align:center;" >
    <a href="" onclick="return false;" data-toggle="modal" data-target="#loginModal" class="btn btn-success">Login</a>
    <!-- <a href="<?php echo base_url();?>" class="btn btn-info">Webpage</a> -->
    <!-- LOGIN modal -->
    <div id="loginModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Please sign in</h4>
          </div>
          <div class="modal-body">
            <form role="form" method="post" action="<?php echo base_url();?>admin/login/doLogin">
                <div class="form-group">
                     <input type="text" class="form-control" placeholder="USERNAME" required autofocus name="username">
                </div>
                <div class="form-group">
                     <input type="password" class="form-control" placeholder="Password" required name="password">
                </div>
                <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
          </div>
<!--
<label class="checkbox">
    <input type="checkbox" value="remember-me"> Remember me
</label>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="button" class="btn btn-primary">Save changes</button>
</div>
-->
        </div>
      </div>
    </div>
    <!-- End login modal -->
</div>