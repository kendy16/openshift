<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pub{
    function updateAction($url){
        ?>
        <a class="btn btn-sm btn-info noLine" href="<?php echo $url; ?>">
            <span class="glyphicon glyphicon-pencil"></span>
        </a>
        <?php
    }

    function copyAction($url) {
        ?>
        <a class="btn btn-sm btn-primary noLine" href="<?php echo $url; ?>">
            <span class="glyphicon glyphicon-retweet"></span>
        </a>
        <?php
    }

    function deleteAction($url, $text){
        ?>
        <a class="btn btn-sm btn-danger noLine showConfirm" href="<?php echo $url; ?>"
       title="<?php echo $text; ?>"><span class="glyphicon glyphicon-remove"></span></a>
        <?php
    }

    function upAction($url){
        ?>
        <a class="btn btn-sm btn-success noLine" href="<?php echo $url; ?>">
            <span class="glyphicon glyphicon-open"></span>
        </a>
        <?php
    }

    function downAction($url){
        ?>
        <a class="btn btn-sm btn-warning noLine" href="<?php echo $url; ?>">
            <span class="glyphicon glyphicon-save"></span>
        </a>
        <?php
    }

    function doSendMail($toMail, $subject, $message, $email){
        if(empty($subject)){
            $info = 'Subject Reply is Empty !';
        }else if(empty($message)){
            $info = 'Content Reply is Empty !';
        }else{
            // Always set content-type when sending HTML email
            $headers = "MIME-Version: 1.0" . "\r\n";
            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

            $headers .= 'From: '.$email;
            mail($toMail, $subject, $message, $headers);
            
            $error=array(
                'call'=>'success',
                'info'=>'E-Mail has been Sent !'
            );
            return $error;
        }
        $error=array(
            'call'=>'error',
            'info'=>'E-Mail failed be sent !'
        );
        return $error;
    }
}