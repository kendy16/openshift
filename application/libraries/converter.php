<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Converter{
	function roundFormatNumber($num, $rounding=-3){
        //round up 3 digit from right (117 -> 1000) and then format it into number format
        return $this->formatNumber(round($num,$rounding));
    }
    function roundNumber($num ,$rounding=-3){
    	return round($num, $rounding);
    }
    function formatNumber($num){
        return number_format($num, 0, ",",".");
    }
    function formatDate($originalDate){
        return date("d M Y", strtotime($originalDate));
    }
}