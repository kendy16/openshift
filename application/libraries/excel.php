<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	class excel{ 
	
		/* sample read n import
		$this->load->library("excel");
		$this->excel->load(APPPATH."xls/invoice.xls");
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->SetCellValue('B2', "whatever");
		$this->excel->save(APPPATH."xls/invoice2.xls");
		
		
		$this->load->library('validator');
		$targetPath =APPPATH."xls/invoice.xls";
        $fileName = $_FILES['backupFile']['name'];
       
		$eInfo='';
        if($fileName=="") $eInfo = 'File must be Filled';
        else if($this->validator->checkLastStr($fileName,array('xls'))==false) $eInfo = 'File must be .xls';
        else if(move_uploaded_file($_FILES['backupFile']['tmp_name'], $targetPath)) {
			$this->load->library("excel");
			$this->excel->load(APPPATH."xls/invoice.xls");
			$data=$this->excel->getActiveSheet();
			
			$totalRow=$data->getCell('C2')->getValue();
			$highestRow = $data->getHighestRow();
			$highestColumn = PHPExcel_Cell::columnIndexFromString($data->getHighestColumn());
			//setting key
			for ($row = 2; $row <=$highestRow; $row++){
				for ($col = 1; $col <=$highestColumn; $col++){
					$namaColom = $this->excel->intToAscii($col);
					$namaColom=$namaColom.strval($row);
					$rowData[$row][$col]=$data->getCell($namaColom)->getValue();
				}
			}
		}
		*/
		
		public function intToAscii($i){
			$ascii=array(
				1=>'A',
				2=>'B',
				3=>'C',
				4=>'D',
				5=>'E',
				6=>'F',
				7=>'G',
				8=>'H',
				9=>'I',
				10=>'J',
				11=>'K',
				12=>'L',
				13=>'M',
				14=>'N',
				15=>'O',
				16=>'P',
				17=>'Q',
				18=>'R',
				19=>'S',
				20=>'T',
				21=>'U',
				22=>'V',
				23=>'W',
				24=>'X',
				25=>'Y',
				26=>'Z'
			);
			
			return $ascii[$i];
		}
		
		private $excel;
		public function __construct() {
			// initialise the reference to the codeigniter instance
			require_once APPPATH.'third_party/PHPExcel.php';
			$this->excel = new PHPExcel();    
		}

		public function load($path) {
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
			$this->excel = $objReader->load($path);
		}

		public function save($path) {
			// Write out as the new file
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save($path);
		}

		public function stream($filename) {       
			header('Content-type: application/ms-excel');
			header("Content-Disposition: attachment; filename=\"".$filename."\""); 
			header("Cache-control: private");        
			$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
			$objWriter->save('php://output');    
		}

		public function  __call($name, $arguments) {  
			// make sure our child object has this method  
			if(method_exists($this->excel, $name)) {  
				// forward the call to our child object  
				return call_user_func_array(array($this->excel, $name), $arguments);  
			}  
			return null;  
		}  
    }