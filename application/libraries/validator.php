<?php

    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Validator{
        /*
         * Tes Regular Expression pattern with help of preg_match, return boolean
         */
        private function tesPattern($pattern, $str){
            if(preg_match($pattern, $str)==1) return true;
            else return false;
        }
        /*
         * check whether string only contain alphabetic character, return boolean
         */
        public function isAlpha($str){
            $pattern = '/^[A-Za-z\s]+$/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check whether string have whitespace
         */
        public function containSpace($str){
            $pattern = '/\s/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check whether string only container numeric character, return boolean
         */
        public function isDigit($str){
            $pattern = '/^[0-9]+$/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check if is float number
         */
        public function isFloat($str){
            $pattern = '/^[0-9]+[\.0-9]*$/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check email address format, based of RCFs
         */
        public function isEmail($str){
            $pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check format of date with format of yyyy-mm-dd, return boolean
         */
        public function isDate($str){
            $pattern = '/^\d{4}-[0|1][0-9]-[0-3][0-9]$/';
            return $this->tesPattern($pattern, $str);
        }
        /*
         * check length of the string, specify min-character and max-character, return boolean
         */
        public function validRange($str, $min, $max){
            if(strlen($str)<$min || strlen($str)>$max) return false;
            else return true;
        }
        /*
         * check whether string characters bellow min range allowed, return boolean
         */
        public function validMinRange($str, $min){
            if(strlen($str)<$min) return false;
            else return true;
        }
        /*
         * check the last string characters, needle can be more than one
         * search in hay
         */
        public function checkLastStr($hay, $needles){
            $check = '';
            if(is_array($needles)){
                foreach($needles as $needle){
                    $check .= '\.'.$needle.'|';
                }
                $check = substr_replace($check ,"",-1);
            }else{
                $check = '\.'.$needles;
            }
            $pattern = "/$check$/";
            return $this->tesPattern($pattern, $hay);
        }
        /*
         * check if there are any empty value of mixed var
         */
        public function anyEmpties($arr=array()){
            foreach($arr as $data){
                if($data=='') return true;
            }
            return false;
        }
    }