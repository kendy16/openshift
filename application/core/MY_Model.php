<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    /*
    ****** [table] = table name ******
    ||===============||
    ||ROLES DATA BASE||
    ||===============||
    1. primary key should like [table]_id
    2. foreign key should like fk_[table]_id, and should have [table]_title in foreign table
    3. normal column should be [table]_[column]
    4. password column / md5 at server should be [table]_[column]_password / [table]_password
    5. secure column should be [table]_[column]_secure
    6. image column should be [table]_[column]_image / [table]_image
    7. other column like
        - date, will be date picker
        - text, will be text area
        - tinyint/boolean, will be radio
        - varchar, will be text
        - int/double/float, will be text
    */
    class MY_Model extends CI_Model{
        protected $query = null;
        protected $result = null;
        function __construct(){

        }
        public function getListOfTables(){
            $tables = $this->db->list_tables();
            return $tables;
        }

        public function selectTotalQuantity($tableName, $where) {
            $column = $tableName."_quantity";
            $query = $this->db->query("SELECT SUM($column) as amount FROM $tableName where $where");
            $count = $query->result_array()[0]["amount"];
            return $count;
        }
        public function selectTotalPrice($tableName, $where) {
            $column = $tableName."_price";
            $query = $this->db->query("SELECT SUM($column) as amount FROM $tableName where $where");
            $count = $query->result_array()[0]["amount"];
            return $count;
        }

        public function select($tableName, $where = NULL, $order = NULL , $foreign = true){
            //field name etc
            $fields = $this->db->field_data($tableName);
            $foreignKeys = array();
            if($foreign) {
                foreach($fields as $field) {
                    $fieldName = $field->name;
                    if(strpos($fieldName, 'fk_') !== FALSE) {
                        $foreignKeys[] = $fieldName;
                    }
                }
            }

            $query= $this->db->select()->from($tableName);
            if(count($foreignKeys) > 0) {
                foreach ($foreignKeys as $fk) {
                    $fkTable = $fk;
                    $fkTable = str_replace("fk_", "", $fkTable);
                    $fkTable = str_replace("_id", "", $fkTable);
                    $fkCondtion = $tableName.".$fk = $fkTable.".$fkTable."_id";
                    $query = $query->join($fkTable,$fkCondtion);
                }
            }
            if(isset($where)) $query = $query->where($where);
            if(isset($order)) $query = $query->order_by($order);
			if($tableName == "sell" || $tableName == "buy") {
				$this->db->limit('50');
				$this->db->order_by($tableName.'_id', 'desc');
			}
            return $query->get()->result();
        }
        public function selectById($tableName, $id, $foreign = true) {
            $datas = $this->select($tableName, array($tableName."_id" =>$id), null, $foreign);
            return @$datas[0];
        }
        public function addOrUpdate($tableName, $record) {
            $record = (array) $record;
            if (@$record[$tableName."_id"] == NULL){
                $this->insert($tableName, $record);
                $message = "New $tableName has successfully added";
            } else {
                // make where more independent check the field
                $message = $this->updateById($tableName, $record, $record[$tableName."_id"]);
            }
            return $message;
        }
        public function updateById($tableName, $record, $id) {
            $this->update($tableName, $record, array($tableName.'_id' => $id));
            $message = "$tableName has successfully updated";
            return $message;
        }
        public function deleteById($tableName, $id){
            $this->db->delete($tableName, array($tableName.'_id'=>$id));
        }
        public function delete($tableName, $where = array()){
            $this->db->delete($tableName, $where);  
        }

        //PRIVATE FUNCTION
        private function insert($tableName, $record=array()){
            $this->db->insert($tableName, $record);
        }
        private function update($tableName, $fields=array(), $where){
            $this->db->update($tableName, $fields, $where);
        }
        private function where($condition=NULL){
                if(is_array($condition)) $this->query = $this->query->where($condition);
                else $this->query = $this->query->where($condition,NULL,FALSE);
        }

        /*Old code
        protected function getTableName(){
            return strtolower(get_class($this));
        }
        public function finalize($data, $id=NULL){
            $msg = $this->tblName;
            if($id==NULL){
                $this->insert($data);
                $msg ="New $msg has successfully added";
            }else{
                $this->updateById($data, $id);
                $msg .=' has successfully updated ';
            }
            return $msg;
        }
        public function insert($record=array()){
            $this->db->insert($this->tblName,$record);
        }
        public function updateById($fields=array(), $id){
            $this->db->update($this->tblName, $fields, array('id'=>$id));
        }
		public function update($fields=array(),$conds=array()){
            $this->db->update($this->tblName, $fields, $conds);
        }
        public function selectById($id){
            // return false if record not found or the record is more than one
            $query = $this->db->get_where($this->tblName, array('id'=>$id));
            if($query->num_rows()==1){
                return $query->row();
            }else return false;
        }
        public function select($where=NULL,$order=NULL){
            $this->query= $this->db->select()->from($this->tblName);
            if(isset($where)) $this->where($where);
            if(isset($order)) $this->query = $this->query->order_by($order);
            return $this->query->get()->result();
        }
        public function deleteById($id){
            $this->db->delete($this->tblName, array('id'=>$id));
        }
		public function delete($where=array()){
			$this->db->delete($this->tblName, $where);	
		}
        public function join($tbls=array(), $tblConditions=array(), $whereConditions=array(), $selectFields=NULL){
            $query = $this->db;
            if(isset($selectFields)) $query = $query->select("*, $selectFields",FALSE);
            else $query = $query->select();

            $query = $query->from($this->tblName);
            for($i=0; $i<count($tbls); $i++){
                $query = $query->join($tbls[$i],$tblConditions[$i]);
            }
            if(isset($whereConditions) && !empty($whereConditions)) $query = $query->where($whereConditions);
            return $query->get()->result();
        }
        public function getSum($field, $where=array(), $joins=NULL){
            $this->query = $this->db->select_sum($field)->from($this->tblName);
            if(isset($joins)){
                foreach($joins as $join){
                    $this->query = $this->query->join($join['tbl'],$join['cond']);
                }
            }
            if(isset($where)) $this->where($where);
            $this->query = $this->query->get();
            if($this->query->num_rows()==1){
                return $this->query->row()->$field;
            }
            return 0;
        }
        public function getQueryCount($where=array(), $joins=NULL, $distinctColumn=NULL){
            $this->query = $this->db;
            if(isset($distinctColumn)) $this->query=$this->query->distinct();
            $this->query = $this->query->select()->from($this->tblName);
            if(isset($joins)){
                foreach($joins as $join){
                    $this->query = $this->query->join($join['tbl'],$join['cond']);
                }
            }
            if(isset($where)) $this->where($where);
            if(isset($distinctColumn)) $this->query = $this->query->group_by($distinctColumn);
            return $this->query->get()->num_rows();
        }*/
    }