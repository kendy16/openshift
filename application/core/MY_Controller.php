<?php
    if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class MY_Controller extends CI_Controller{ 
        //this for key session
        var $keySession = 'Admin-InterfaceK3yS3ss10nStockist';
        
        //this for title page and session user
		var $pageName = "home";
        var $navigationName = "";
        var $navigations;
        var $productNavigations;
        var $footerLinks;
        var $userVisited;
		var $userLogin;
		
        //this for header and footer visible
        var $metaKey = "metaKey";
        var $metaDesc = "metaDesc";
        var $metaAuthor = "kendysusantho16@gmail.com";
		
        //this for main template
        var $headerVisible = true;
        var $headerSliderVisible = true;
        var $footerVisible = true;
        var $livechatVisible = false;
		
        //this for footer information
        var $companyName = "THO Motor";
        var $copyright = 2016;
        var $kendyEmail = "mailto:kendysusantho16@gmail.com";
        var $kendyGroup = "Kendy";
		
		//ADMIN AREA
		//navigation active
        var $headerNavigation = "";
        var $projectName = "Stockist Management System";
		
		//ADDITIONAL AREA
		var $productDescription = "";
		
        function __construct(){
            parent::__construct();
            $this->userLogin = $this->session->userdata('userLogin');
            $this->getFooterLinks();
        }

        private function checkLogin(){
            if(!$this->userLogin
                || $this->userLogin['title'] != $this->keySession){
				return false;
            }
			
            return true;
        }
        
        public function template($contentPage, $contentVars=array(), $contentCss=array(), $contentJs=array()){
            $this->load->library('parser');
            
            /*------------
            * css and js
            *------------*/
            $css=$this->getCssString($contentCss);
            $js=$this->getJsString(($contentJs));
            
            //meta data
            $metaKey = $this->metaKey;
            $metaDesc = $this->metaDesc;
            $metaAuthor = $this->metaAuthor;
            
            //error view
            $error='';
            $eOverlay = $this->session->userdata('error');
            if($eOverlay){
                $error = $this->load->view('admin/template/info.php',array('call'=>$eOverlay['call'],'info'=>$eOverlay['info']),true);
                $this->session->unset_userdata('error');
            }

            //setting
            $setting = "";
            if($contentPage != "maintenance.php" && $contentPage != "admin/login.php") {
                $settings = $this->kenmodel->select("settings", array('settings_key' => "Main Template", 'settings_active' => 1));
                foreach ($settings as $item) {
                    $setting .= $item->settings_value;
                }
            }

            $data = array(
                    'css'=>$css,
                    'js'=>$js,
                    'header'=>null,
                    'headerSlider'=>null,
                    'footer'=>null,
                    'content'=>$this->load->view($contentPage, $contentVars, true),
                    'metaDesc'=>$metaDesc,
                    'metaDat'=>$metaKey,
                    'metaAuthor'=>$metaAuthor,
                    'title'=>$this->pageName,
                    'error'=>$error,
                    'setting'=>$setting,
                );


            /*-------------------------
            * Data for Template Class
            *------------------------*/
            if($this->headerVisible) {
                $navigations = $this->navigations;
                $headerData=array(
                    "companyName" => $this->companyName,
                    "navigationName" => $this->navigationName,
                    "navigations" => $navigations,
                );
                $data['header'] = $this->load->view('template/header.php',$headerData, true);
            }
            if($this->headerSliderVisible) {
                $headerSliderData = array();
                if($this->headerSliderVisible) {
                    $headerSliderData['data'] = $this->kenmodel->select("carousel_slider", array('carousel_slider_active' => 1));
                }
                if(count($headerSliderData['data']) > 0) {
                    $data['headerSlider'] = $this->load->view('template/header-slider.php',$headerSliderData, true);
                }
            }
            if($this->footerVisible) {
                $footerData=array(
                    "copyright" => $this->copyright,
                    "companyName" => $this->companyName,
                    "kendyEmail" => $this->kendyEmail,
                    "kendyGroup" => $this->kendyGroup,
                    "footerLinks" => $this->footerLinks,
                    "userVisited" => $this->userVisited,
                    "livechatVisible" => $this->livechatVisible,
                );
                $data['footer'] = $this->load->view('template/footer.php',$footerData, true);
            }
            /*-------------------------
            * End of Data for Template Class
            *------------------------*/
            
            $this->parser->parse('template/main.php',$data);
        }
        
		public function templateAdmin($contentPage, $contentVars=array(), $contentCss=array(), $contentJs=array()){
            /* check login */
            if($this->checkLogin()==false) {
                redirect(base_url().'admin/login');
            }
			
			$this->load->library('parser');
            
            /*------------
            * css and js
            *------------*/
            $css=$this->getCssString($contentCss);
            $js=$this->getJsString(($contentJs));

            /*-------------------------
            * Data for Template Class
            *------------------------*/ 
            $this->load->model("kenmodel");
			$navigations = $this->kenmodel->getListOfTables();
			
			
			$navData=array(
				'navigations'=>$navigations,
				'username'=>$this->userLogin['username'],
                'user_id'=>$this->userLogin['id'],
                'active_navigation'=>$this->headerNavigation,
			);
			
			$headerData=array(
                "projectName" => $this->projectName,
                "projectLink" => base_url()."admin/home",
                "backupLink" => base_url()."admin/mypanel/backup",
                "headerNavigation" => $this->headerNavigation,
            );
			$footerData=array(
                "copyright" => $this->copyright,
                "companyName" => $this->companyName,
                "kendyEmail" => $this->kendyEmail,
                "kendyGroup" => $this->kendyGroup,
                "footerLinks" => $this->footerLinks,
            );
            /*-------------------------
            * End of Data for Template Class
            *------------------------*/
            //error view
			$error='';
            $eOverlay = $this->session->userdata('error');
            if($eOverlay){
                $error = $this->load->view('admin/template/info.php',array('call'=>$eOverlay['call'],'info'=>$eOverlay['info']),true);
                $this->session->unset_userdata('error');
            }
			
            //setting
            $setting = "";

            $data = array(
                    'css'=>$css,
                    'js'=>$js,
                    'header'=>$this->load->view('admin/template/header.php', $headerData, true),
                    'footer'=>$this->load->view('admin/template/footer.php', $footerData, true),
                    'leftNav'=>$this->load->view('admin/template/leftNav.php', $navData, true),
                    'content'=>$this->load->view($contentPage, $contentVars, true),
                    'title'=>$this->pageName,
					'error'=>$error,
                    'emailForm'=>$this->load->view('admin/template/email.php', null, true),
                    'setting'=>$setting,
                );


            $this->parser->parse('admin/template/main.php',$data);
        }     

        public function deleteRecord($table,$id,$page,$folder=NULL){
            $this->load->model("kenmodel");
            $this->kenmodel->deleteById($table, $id);
            if(!empty($folder)){
                $temp = $this->$table->selectById($id);
                $oldImage = './img/'.$folder.'/'.$temp->image;
                if(!unlink($oldImage)){
                    $this->session->set_userdata('error',array('call'=>'error','info'=>'failed in deleting image'));
                    return;
                }
            }
            $this->session->set_userdata('error',array('call'=>'success','info'=>"$table data has been successfully deleted"));

            redirect(base_url().'admin/mypanel/panel/'.$page);
        }

        private function getFooterLinks (){
            $this->load->model('kenmodel');
            $footerLink = $this->kenmodel->select('social_link', array('social_link_active' => 1, ));
            $this->footerLinks = $footerLink;
        }
        
        private function getCssString($css=NULL){
            $str=NULL;
            if(isset($css)){
                foreach($css as $link){
                    $str.="<link rel=\"stylesheet\" type=\"text/css\" href=\"".base_url()."css/$link\"/>";
                }
            }
            return $str;
        }
        
        private function getJsString($js=NULL){
            $str=NULL;
            if(isset($js)){
                foreach($js as $link){
                    $str.="<script type=\"text/javascript\" src=\"".base_url()."js/$link\"></script>";
                }
            }
            return $str;
        }
		
        /*OLD OR NOT USED*/
        protected function popup($contentPage, $navigation=array(array('link'=>'','text'=>'','selected'=>TRUE)), $contentVars=NULL, $contentCss=array(), $contentJs=array(), $remarks=NULL){
            $this->load->library('parser');
            /*------------
            * css and js
            *------------*/
            $css=$this->getCssString($contentCss);
            $js=$this->getJsString($contentJs);

            /*-------------------------
            * Data for Template Class
            *------------------------*/
            $data = array(
                'css'=>$css,
                'js'=>$js,
                'navigation'=>$navigation,
                'content'=>$this->load->view($contentPage, $contentVars, TRUE),
                'remarks'=>$remarks
            );
            $this->parser->parse('admin/template/popup.php',$data);
        }
    }